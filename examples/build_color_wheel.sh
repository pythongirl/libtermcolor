#!/bin/sh

CC=clang
CFLAGS="-I.. -lm -Wall"
OUTPUT="color_wheel.out"
SOURCES="color_wheel.c ../libtermcolor.c"

$CC -o $OUTPUT $SOURCES $CFLAGS
