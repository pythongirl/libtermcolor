#!/bin/sh

CC=clang
CFLAGS="-I.. -lm -Wall"
OUTPUT="rainbow.out"
SOURCES="rainbow.c ../libtermcolor.c"

$CC -o $OUTPUT $SOURCES $CFLAGS
