/*
    MIT License
    Copyright (c) 2018 Graham Scheaffer

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to
    deal in the Software without restriction, including without limitation the
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "libtermcolor.h"

int main(int argc, char **argv){
    Color *c = malloc(sizeof(Color));

    double radius = 8; // Change this for a bigger or smaller circle

    if(argc >= 2){
        radius = atof(argv[1]);
        if(radius == 0){
            printf("Usage: %s <radius>\nRadius must be larger than zero.\n", argv[0]);
            return 1;
        }
    }

    for(double y = -radius; y <= radius; y++){
        for(double x = -radius; x <= radius; x++){
            double hue = atan2(y, x) / M_PI / 2 + 0.5;
            double saturation = sqrt(x*x + y*y) / radius;
            if(saturation > 1.0){
                printf("  ");
                continue;
            }

            set_hsv_color(c, hue, saturation, 1.0);
            print_bgcolor("  ", c);
        }
        printf("\n");
    }

    free(c);
    return 0;
}
